# Application name: TreasureTroves

## User Credentials
* Admin User
  * email: admin@mail.com
  * password: admin123
* Dummy Customer
  * email: jasper@gmail.com
  * password: 987654321

## Features

**User Resources**
* User registration
* User login/authentication

**Admin Dashboard**
* Create product
* Update product information
* Retrieve all prodcuts
* Deactivate/Activate products

**User Products Catalog Page**
* View all products
* Retrieve single product

**Checkout Order**
* Add-to-cart functionality
* Clear cart functionality
* Non-admin User checkout (Create Order)
* Retrieve authenticated user's order
* Retrieve all orders (Admin only)